class SamlController < ApplicationController
  skip_before_action :verify_authenticity_token, only: :consume

  def verify_access
    unless session[:userid]
      redirect_to login_path
    end
  end

  def init
    request = OneLogin::RubySaml::Authrequest.new
    redirect_to(request.create(saml_settings), allow_other_host: true)
  end

  def consume
    response = OneLogin::RubySaml::Response.new(params[:SAMLResponse], :settings => saml_settings)
    if response.is_valid?
      session[:userid] = response.nameid
      session[:attributes] = response.attributes

      redirect_to protected_path
    else
      raise response.errors.inspect
    end
  end

  def metadata
    settings = saml_settings
    meta = OneLogin::RubySaml::Metadata.new
    render :xml => meta.generate(settings), :content_type => "application/samlmetadata+xml"
  end

  private

  def saml_settings
    idp_metadata_parser = OneLogin::RubySaml::IdpMetadataParser.new
    settings = idp_metadata_parser.parse_remote("https://dev-88244665.okta.com/app/exkgba6eiwxSCpRJB5d7/sso/saml/metadata")

    settings.assertion_consumer_service_url = "https://#{request.host}/consume"
    settings.sp_entity_id                   = "https://#{request.host}/metadata"
    settings.name_identifier_format         = "urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress"

    settings
  end
end
