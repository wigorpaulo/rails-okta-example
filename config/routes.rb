Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'login', to: 'login#index'
  get 'protected', to: 'protected_resource#index'

  get 'init', to: 'saml#init'
  get 'metadata', to: 'saml#metadata'
  post 'consume', to: 'saml#consume'
end
